package ru.bokhan.tm.bootstrap;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.bokhan.tm.api.IEndpointLocator;
import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.constant.ReflectionConstant;
import ru.bokhan.tm.endpoint.*;
import ru.bokhan.tm.exception.system.UnknownCommandException;
import ru.bokhan.tm.util.TerminalUtil;

import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

@Getter
@Setter
public final class Bootstrap implements IEndpointLocator {

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Nullable
    private SessionDTO currentSession;

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    @NotNull
    final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @NotNull
    private final TaskEndpointService taskEndpointService = new TaskEndpointService();

    @NotNull
    final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    @NotNull
    private final UserEndpointService userEndpointService = new UserEndpointService();

    @NotNull
    final UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();

    @NotNull
    private final DomainEndpointService domainEndpointService = new DomainEndpointService();

    @NotNull
    final DomainEndpoint domainEndpoint = domainEndpointService.getDomainEndpointPort();

    @NotNull
    private final ServerInfoEndpointService serverInfoEndpointService = new ServerInfoEndpointService();

    @NotNull
    final ServerInfoEndpoint serverInfoEndpoint = serverInfoEndpointService.getServerInfoEndpointPort();

    private void initCommands() throws IllegalAccessException, InstantiationException {
        @NotNull final Reflections reflections = new Reflections(ReflectionConstant.COMMAND_PACKAGE_PATH);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            registry(clazz.newInstance());
        }
    }

    private void registry(@Nullable final AbstractCommand command) {
        if (command == null) return;
        command.setEndpointLocator(this);
        commands.put(command.name(), command);
    }

    public void run(@Nullable final String[] arguments) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        try {
            if (parseArguments(arguments)) System.exit(0);
            initCommands();
        } catch (Exception e) {
            logError(e);
        }
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                logError(e);
            }
        }
    }

    private void logError(@NotNull final Exception e) {
        System.err.println(e.getMessage());
        System.err.println("[FAIL]");
    }

    public void parseArgument(@Nullable final String argument) throws Exception {
        if (argument == null || argument.isEmpty()) return;
        @Nullable final AbstractCommand command = getCommandByArgument(argument);
        if (command == null) return;
        command.execute();
    }

    public void parseCommand(@Nullable final String cmd) throws Exception {
        if (cmd == null || cmd.isEmpty()) return;
        @Nullable final AbstractCommand command = commands.get(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        command.execute();
    }

    public boolean parseArguments(@Nullable final String[] arguments) throws Exception {
        if (arguments == null || arguments.length == 0) return false;
        @Nullable final String argument = arguments[0];
        parseArgument(argument);
        return true;
    }

    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @Nullable
    private AbstractCommand getCommandByArgument(@Nullable final String argument) {
        if ((argument == null) || argument.isEmpty()) return null;
        for (@NotNull final AbstractCommand command : commands.values()) {
            if (argument.equals(command.argument())) return command;
        }
        return null;
    }

}