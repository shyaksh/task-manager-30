package ru.bokhan.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.api.repository.ITaskRepository;
import ru.bokhan.tm.dto.TaskDTO;
import ru.bokhan.tm.entity.Task;

import javax.persistence.EntityManager;
import java.util.List;

public final class TaskRepository extends AbstractRepository<TaskDTO> implements ITaskRepository {

    public TaskRepository(@NotNull EntityManager em) {
        super(em);
    }

    @Override
    public @NotNull List<TaskDTO> findAll() {
        return em.createQuery("SELECT e FROM TaskDTO e", TaskDTO.class)
                .getResultList();
    }

    @Nullable
    @Override
    public TaskDTO findById(@NotNull String id) {
        return em.createQuery("SELECT e FROM TaskDTO e WHERE e.id = :id", TaskDTO.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public long count() {
        return em.createQuery("SELECT COUNT(e) FROM TaskDTO e", Long.class).getSingleResult();
    }


    @Override
    public void clear(@NotNull final String userId) {
        em.createQuery("DELETE FROM TaskDTO e WHERE e.userId = :userId", TaskDTO.class)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(@NotNull final String userId) {
        return em.createQuery("SELECT e FROM TaskDTO e WHERE e.userId = :userId", TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public TaskDTO findById(@NotNull final String userId, @NotNull final String id) {
        return em.createQuery("SELECT e FROM TaskDTO e WHERE e.id = :id AND e.userId = :userId", TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Nullable
    @Override
    public TaskDTO findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final List<TaskDTO> list = em.createQuery("SELECT e FROM TaskDTO e WHERE e.userId = :userId", TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
        if (index > list.size()) return null;
        return list.get(index);
    }

    @Nullable
    @Override
    public TaskDTO findByName(@NotNull final String userId, @NotNull final String name) {
        return em.createQuery("SELECT e FROM TaskDTO e WHERE e.userId = :userId AND e.name = :name", TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final TaskDTO task = findById(userId, id);
        if (task == null) return;
        remove(task);
    }

    @Override
    public void removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final TaskDTO task = findByIndex(userId, index);
        if (task == null) return;
        remove(task);
    }

    @Override
    public void removeByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final TaskDTO task = findByName(userId, name);
        if (task == null) return;
        remove(task);
    }

    @Override
    public void remove(@NotNull final TaskDTO dto) {
        @Nullable final Task task = em.createQuery("SELECT e FROM Task e WHERE e.id = :id", Task.class)
                .setParameter("id", dto.getId())
                .getSingleResult();
        if (task == null) return;
        em.remove(task);
    }

}