package ru.bokhan.tm.command.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.endpoint.DomainEndpoint;
import ru.bokhan.tm.endpoint.SessionDTO;
import ru.bokhan.tm.exception.security.AccessDeniedException;

public final class DataXmlLoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-xml-load";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from xml file.";
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO session = endpointLocator.getCurrentSession();
        if (session == null) throw new AccessDeniedException();
        System.out.println("[DATA XML LOAD]");
        @NotNull final DomainEndpoint domainEndpoint = endpointLocator.getDomainEndpoint();
        if (domainEndpoint.loadFromXml(session)) System.out.println("[OK]");
    }

}
