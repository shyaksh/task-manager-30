package ru.bokhan.tm.bootstrap;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.api.IServiceLocator;
import ru.bokhan.tm.api.endpoint.*;
import ru.bokhan.tm.api.service.*;
import ru.bokhan.tm.dto.ProjectDTO;
import ru.bokhan.tm.dto.UserDTO;
import ru.bokhan.tm.endpoint.*;
import ru.bokhan.tm.enumerated.Role;
import ru.bokhan.tm.exception.system.NotFoundException;
import ru.bokhan.tm.service.*;

import javax.xml.ws.Endpoint;

@Getter
public final class Bootstrap implements IServiceLocator {


    @NotNull
    private final IUserService userService = new UserService(this);


    @NotNull
    private final ITaskService taskService = new TaskService(this);

    @NotNull
    private final IProjectService projectService = new ProjectService(this);

    @NotNull
    private final IDomainService domainService = new DomainService(
            userService, projectService, taskService
    );

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ISessionService sessionService = new SessionService(this
    );

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IServerInfoEndpoint serverInfoEndpoint = new ServerInfoEndpoint(this);

    @NotNull
    private final IEntityManagerService entityManagerService = new EntityManagerService(this);

    private void initData() {
        userService.create("user", "user", "user@test.ru");
        @Nullable final UserDTO user = userService.findByLogin("user");
        if (user == null) throw new NotFoundException();
        projectService.create(user.getId(), "UserProject1");
        projectService.create(user.getId(), "UserProject2");
        @Nullable final ProjectDTO userProject1 = projectService.findByName(user.getId(), "UserProject1");
        if (userProject1 == null) throw new NotFoundException();
        taskService.create(user.getId(), userProject1.getId(), "UserTask1");
        taskService.create(user.getId(), userProject1.getId(), "UserTask2");
        userService.create("admin", "admin", Role.ADMIN);
        @Nullable final UserDTO admin = userService.findByLogin("admin");
        if (admin == null) throw new NotFoundException();
        projectService.create(admin.getId(), "AdminProject1");
        projectService.create(admin.getId(), "AdminProject2");
        @Nullable final ProjectDTO adminProject2 = projectService.findByName(admin.getId(), "AdminProject2");
        if (adminProject2 == null) throw new NotFoundException();
        taskService.create(admin.getId(), adminProject2.getId(), "AdminTask1");
        taskService.create(admin.getId(), adminProject2.getId(), "AdminTask2");
    }

    private void initEndpoint() {
        registry(sessionEndpoint);
        registry(taskEndpoint);
        registry(projectEndpoint);
        registry(userEndpoint);
        registry(domainEndpoint);
        registry(serverInfoEndpoint);
    }

    public void initEntityManagerFactory() {
        entityManagerService.init();
    }

    public void initProperty() {
        propertyService.init();
    }

    private void registry(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        @NotNull String host = propertyService.getServerHost();
        @NotNull Integer port = propertyService.getServerPort();
        @NotNull String name = endpoint.getClass().getSimpleName();
        @NotNull String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    public void run(@Nullable final String[] arguments) {
        System.out.println("** TASK MANAGER SERVER **");
        initProperty();
        initEntityManagerFactory();
        //initData();
        initEndpoint();
    }

}