package ru.bokhan.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.bokhan.tm.marker.IntegrationCategory;

import java.util.List;

@Category(IntegrationCategory.class)
public final class SessionEndpointTest {

    @NotNull
    private final SessionEndpoint endpoint = new SessionEndpointService().getSessionEndpointPort();


    @Test
    public void closeSession() {
        @NotNull final SessionDTO userSession = endpoint.openSession("user", "user");
        endpoint.closeSession(userSession);
    }

    @Test(expected = Exception.class)
    public void closeSessionNegative() {
        @NotNull final SessionDTO userSession = endpoint.openSession("user", "user");
        endpoint.closeSession(userSession);
        endpoint.closeSession(userSession);
    }

    @Test
    public void closeSessionAll() {
        @NotNull final SessionDTO adminSession = endpoint.openSession("admin", "admin");
        @NotNull final SessionDTO userSession = endpoint.openSession("user", "user");
        endpoint.closeSessionAll(adminSession);
    }

    @Test(expected = Exception.class)
    public void closeSessionAllNegative() {
        @NotNull final SessionDTO adminSession = endpoint.openSession("admin", "admin");
        @NotNull final SessionDTO userSession = endpoint.openSession("user", "user");
        endpoint.closeSessionAll(adminSession);
        endpoint.closeSessionAll(adminSession);
    }

    @Test
    public void getUserBySession() {
        @NotNull final SessionDTO adminSession = endpoint.openSession("admin", "admin");
        @NotNull final UserDTO user = endpoint.getUserBySession(adminSession);
        Assert.assertEquals("admin", user.getLogin());
        endpoint.closeSession(adminSession);
    }

    @Test
    public void getUserIdBySession() {
        @NotNull final SessionDTO adminSession = endpoint.openSession("admin", "admin");
        @NotNull final UserDTO user = endpoint.getUserBySession(adminSession);
        @NotNull final String id = endpoint.getUserIdBySession(adminSession);
        Assert.assertEquals(user.getId(), id);
        endpoint.closeSession(adminSession);
    }

    @Test
    public void findSessionAll() {
        @NotNull final SessionDTO adminSession = endpoint.openSession("admin", "admin");
        @NotNull final SessionDTO userSession = endpoint.openSession("admin", "admin");
        @NotNull final List<SessionDTO> sessions = endpoint.findSessionAll(adminSession);
        Assert.assertEquals(2, sessions.size());
        endpoint.closeSessionAll(adminSession);
    }

    @Test
    public void openSession() {
        @NotNull final SessionDTO adminSession = endpoint.openSession("admin", "admin");
        Assert.assertNotNull(adminSession);
        Assert.assertEquals("admin", endpoint.getUserBySession(adminSession).getLogin());
        endpoint.closeSessionAll(adminSession);
    }

    @Test
    public void signOutByLogin() {
        @NotNull final SessionDTO adminSession = endpoint.openSession("admin", "admin");
        @NotNull final SessionDTO userSession = endpoint.openSession("user", "user");
        endpoint.signOutByLogin(adminSession, "user");
        Assert.assertEquals(1, endpoint.findSessionAll(adminSession).size());
        endpoint.closeSessionAll(adminSession);
    }

    @Test
    public void signOutByUserId() {
        @NotNull final SessionDTO adminSession = endpoint.openSession("admin", "admin");
        @NotNull final SessionDTO userSession = endpoint.openSession("user", "user");
        @NotNull final String userId = userSession.getUserId();
        endpoint.signOutByUserId(adminSession, userId);
        Assert.assertEquals(1, endpoint.findSessionAll(adminSession).size());
        endpoint.closeSessionAll(adminSession);
    }

    @Test
    public void removeSessionAll() {
        @NotNull final SessionDTO adminSession = endpoint.openSession("admin", "admin");
        @NotNull final SessionDTO userSession = endpoint.openSession("user", "user");
        endpoint.closeSessionAll(adminSession);
    }

    @Test(expected = Exception.class)
    public void removeSessionAllNegative() {
        @NotNull final SessionDTO adminSession = endpoint.openSession("admin", "admin");
        @NotNull final SessionDTO userSession = endpoint.openSession("user", "user");
        endpoint.closeSessionAll(adminSession);
        endpoint.findSessionAll(adminSession);
    }

    @Test
    public void removeSession() {
        @NotNull final SessionDTO adminSession = endpoint.openSession("admin", "admin");
        @NotNull final SessionDTO userSession = endpoint.openSession("user", "user");
        endpoint.findSessionAll(adminSession);
        endpoint.closeSession(userSession);
        Assert.assertEquals(1, endpoint.findSessionAll(adminSession).size());
        endpoint.closeSessionAll(adminSession);
    }

}