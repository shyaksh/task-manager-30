package ru.bokhan.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.bokhan.tm.api.service.*;

public interface IServiceLocator {

    @NotNull
    IUserService getUserService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    ISessionService getSessionService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IEntityManagerService getEntityManagerService();

}
